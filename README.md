# MADatPars_Nadine

## Name

Masterarbeit Datei-Parser Nadine

## Description

This script converts patient measurments from Excel to CSV and removes unwanted information.
After all it combines the information to one big file.

## Installation

Copy the script to your computer. You're done.

## Configuration

Put your fields to filter on per line into `RetainFields.txt` in the Format "Analysis Group - Analysis Name", eg. "Hämatogramm - Thrombozyten".

_Note_: `RetainFields.txt` needs to be encoded ASCII and not UTF8 for the Umlauts to work. A trailing newline is required after each line.

## Usage

Make sure to put all your files to parse into the folder "input".
Execute MADatPars_Nadine.ps1 with powershell and you should be fine.

_Note_: If the Execution Policy is not set to unrestricted, you will need to execute the script as follows:

```powershell
PowerShell.exe -ExecutionPolicy Bypass -File MADatPars_Nadine.ps1
```

_Note_: If Excel remains running after the conversion, you may run `taskkill /IM excel.exe /F`.

Retrieve the filtered files from the folder "filtout".

_Note: You might want to change the configuration, but that's up to you._

## Support

No support will be provided.

## Authors and acknowledgment

Thanks to Michael Heutschi for your valuable Input in creating this utility.

## License

License type is not yet defined.

## Project status

This project currently is under development, though it should be possible to be executed (no liability).

_Note_: This project does not yet tackle the issue of different csv file types (semicolon, comma). Currently only CSV with comma are supported (generally result of english excel). This needs to be addressed before using productively.

## Original Project Requirements

```text
Immer haben: Name, Geburtsdatum, Fallnummer
Zeile mit Daten

Nie haben: Spalten ohne Laborwerte, Station und Uhrzeit

Folgende Werte interessieren: 
- Hämatogramm / Thrombozyten
- Differenzierung maschinell / Neutrophile
- Differenzierung maschinell / Lymphozyten
- Differenzierung visuell / Neutrophile Granulozyten
- Serum: Proteine / IGA
- Serum: Proteine / IGG
- T-Zell Subpopulationen (EDTA-Blut) Flowzytometrie / CD4 absolut

Umgebung:
Windows 10 21H2
Powershell: 5.1.18362.1801
Python 3.4.3 (v3.4.3:9b73f1c3e601, Feb 24 2015, 22:44:40)

```
