<#
# Filename:       MADatPars_Nadine
# Author:         Simon Monai, simon@jongliertricks.ch
# Version:        1.0
# Date:           2022-02-07
# PS min Version: 5.1
# Note:           Please refer to the bottom for detailed instructions
#>


<#
.Synopsis
   Converts patient measurments into the required format
.DESCRIPTION
   This script converts patient measurments from Excel to CSV and removes unwanted information.
   After all it combines the information to one big file.
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
.INPUTS
   Inputs to this cmdlet (if any)
.OUTPUTS
   Output from this cmdlet (if any)
.NOTES
   General notes
.COMPONENT
   The component this cmdlet belongs to
.ROLE
   The role this cmdlet belongs to
.FUNCTIONALITY
   This script converts excel to csv and extracts the relevant information.
#>


#region Get Parameters
param (
    [string]$froot = "$PSScriptRoot",
    [string]$finput = "$froot\input",
    [string]$fcsvout = "$froot\csvout",
    [string]$ffiltout = "$froot\filtout",
    [string]$ffinalout = "$froot\output",
    [string]$fparseout = "$froot\parseout",
    [string]$fexcelcsvout = "$froot\excelout",
    [string]$RetainFieldsPath = "$froot\RetainFields.txt",
    [switch]$help = $false,
    [switch]$whatif
)
#endregion



#region Define Vars
$patient = @{} # ValueArray containing wanted data
$dropline = "" # Linues extracted from file that are useless and can be dropped without issues
$dropcolumn = "" # Values extracted from files that are useless and can be dropped without issues

<# $patient = @{ 
    name = "B�rlauch"; 
    Geburtsdatum = "01011991"; 
    Fallnr = "12343";
    Measurements = @{
        "H�matogramm - Thrombo" = @{
            "20220101" = 42;
            "20211230" = 41;
        };
        "Diff Masch - Neutophy" = @{
            "20211231" = 22;
        }
    }
} #>

#endregion


#region Define Fields to retain
$RetainFields = Get-Content -Path $RetainFieldsPath
#endregion


#region Define Functions
Function ExcelToCsv ($FileName, $inputdir = $finput, $outdir = $fcsvout) {
    $FilePath = "$inputdir\$FileName"
    $Excel = New-Object -ComObject Excel.Application
    $wb = $Excel.Workbooks.Open($FilePath)
	
    foreach ($ws in $wb.Worksheets) {
        $ws.SaveAs("$outdir\" + (Get-Item $FilePath).BaseName + ".csv", 6)
    }
    $Excel.Quit()
}


Function CheckDirs () {
    # Make Sure Directory Exists
    [System.IO.Directory]::CreateDirectory($fcsvout) | Out-Null
    [System.IO.Directory]::CreateDirectory($ffiltout) | Out-Null
    [System.IO.Directory]::CreateDirectory($fparseout) | Out-Null
    [System.IO.Directory]::CreateDirectory($fexcelcsvout) | Out-Null

    # Clean Temporary Dir
    Remove-Item $fcsvout\*
    Remove-Item $fparseout\*
    Remove-Item $ffiltout\*
    Remove-Item $fexcelcsvout\*
}

Function ConvertCSV ($FileName, $indir = $fcsvout)
{
    $FilePath = "$indir\$FileName"

    $line = Get-Content $FilePath | Select-Object -First 1
    $delimiter = if($line.Split(",").Length -gt $line.Split(";").Length){","}else{";"};
    if ( $delimiter -eq "," ) {
        Write-Host "Converting from comma to semicolon based csv: $FilePath"
        #Import-Csv -Path $FilePath -Delimiter $delimiter | Export-Csv -Path "$FilePath.conv" -Delimiter ";" -NoTypeInformation    # This does not retain all fields and breaks umlauts
    }
}

Function ParseCSVtoJSON ($FileName, $indir = $fcsvout, $outdir = $ffiltout)
{
    $FilePath = "$indir\$FileName"
    $OutPath = "$outdir\" + (Get-Item $FilePath).BaseName + ".json"
    $patient = @{}

    $filecontent= Get-Content $FilePath 
    $line = $filecontent | Select-Object -First 1
    $delimiter = if($line.Split(",").Length -gt $line.Split(";").Length){","}else{";"};
    if ( $delimiter -eq ";" ) {
        Write-Host "Please note: Delimiter used is ; which will not work."
    }
    
    # Get Patient information
    $name, $dateob, $casenr, $stationname, $dropline, $dates, $times, $titles, $measurements = $filecontent # Extract useful lines
    $patient["name"] = $name.Split(",")[1] #use regex because bad parsing of csv
    $patient["dateob"] = $dateob.Split(",")[1]
    $patient["casenr"] = $casenr.Split(",")[1]
    $dropline, $dropline, $dropline, $dates = $dates.Split(",") # Drop useless columns

    # Clean Vars before loop
    $filecontent = ""
    $msmt_group = ""
    $msmt_type = ""
    $patient["measurement"] = @{}

    # Iterate through measurements
    foreach ( $m in $measurements) {
        
        # Break if reached end of measurements (do not retain footnotes)
        if ($m[0] -eq ",") {
            break
        }
        
        # Check if it's a analytics group or type
        if ($m + 'EOL' -match "^[A-z������][^,]+,+EOL") { 
            # Get New Analytics Group
            $msmt_group = $m.Split(",")[0]
        }
        else {
            # Get Analytics Type
            $msmt_type = $m.Split(",")[0]
            $msmt_name = "$msmt_group - $msmt_type"
            foreach ( $rf in $RetainFields) {
                if ($msmt_name -eq $rf ) {
                    $patient["measurement"]["$msmt_name"] = @{}
                    $dropline, $dropline, $dropline, $msmts = $m.Split(",")
                    $count = 0;
                    foreach($date in $dates) {
                        # Store relevant Measurement
                        $patient["measurement"]["$msmt_name"]["$date"] = ($msmts[$count]) `
                            -replace '(^)(\*[0-9,]{1,5}\) )([0-9<>])', '$1$3' <# Remove Remarks e.g. *1) #> `
                            -replace '(^)(\*[0-9,]{1,5}\) \!sKomm[ ]?)($)', '$1' <# Remove Excel Errors e.g. !sKomm #> `
                            -replace '([0-9])( [\-\+]{1,2})($)', '$1$3' <# Remove Plus or Minus #>
                        $count += 1
                    }
                }
            }
        }
    }   
    $patient | ConvertTo-JSON | Out-File -FilePath $OutPath
    # Convert Object back to usable table now with ExportFilteredData
}


Function FilterCSV ($FileName, $indir = $fcsvout, $outdir = $ffiltout)
{
    $FilePath = "$indir\$FileName"
    $OutPath = "$outdir\" + (Get-Item $FilePath).BaseName + ".csv"
    $patient = @{}

    $filecontent= Get-Content $FilePath 
    $line = $filecontent | Select-Object -First 1
    $delimiter = if($line.Split(",").Length -gt $line.Split(";").Length){","}else{";"};
    if ( $delimiter -eq ";" ) {
        Write-Host "Please note: Delimiter used is ; which will not work."
    }
    
    # Get Patient information
    $name, $dateob, $casenr, $stationname, $dropline, $dates, $times, $titles, $measurements = $filecontent # Extract useful lines

    
    $patient["name"] = $name.Split(",")[1] #use regex because bad parsing of csv
    Add-Content -Path $OutPath -Value $name
    $patient["dateob"] = $dateob.Split(",")[1]
    Add-Content -Path $OutPath -Value $dateob
    $patient["casenr"] = $casenr.Split(",")[1]
    Add-Content -Path $OutPath -Value $casenr
    Add-Content -Path $OutPath -Value $dates
    Add-Content -Path $OutPath -Value $times
    Add-Content -Path $OutPath -Value $titles
    $dropline, $dropline, $dropline, $dates = $dates.Split(",") # Drop useless columns
    

    # Clean Vars before loop
    $filecontent = ""
    $msmt_group = ""
    $msmt_type = ""
    $patient["measurement"] = @{}

    # Iterate through measurements
    foreach ( $m in $measurements) {
        
        # Break if reached end of measurements (do not retain footnotes)
        if ($m[0] -eq ",") {
            break
        }
        
        # Check if it's a analytics group or type
        if ($m + 'EOL' -match "^[A-z������][^,]+,+EOL") { 
            # Get New Analytics Group
            $msmt_group = $m.Split(",")[0]
        }
        else {
            # Get Analytics Type
            $msmt_type = $m.Split(",")[0]
            $msmt_name = "$msmt_group - $msmt_type"
            foreach ( $rf in $RetainFields) {
                if ($msmt_name -eq $rf ) {
                    $patient["measurement"]["$msmt_name"] = @{}
                    $dropcolumn, $dropcolumn, $dropcolumn, $msmts = $m.Split(",")
                    $count = 0;
                    foreach($date in $dates) {
                        # Store relevant Measurement
                        $patient["measurement"]["$msmt_name"]["$date"] = ($msmts[$count]) `
                            -replace '(^)(\*[0-9,]{1,5}\) )([0-9<>])', '$1$3' <# Remove Remarks e.g. *1) #> `
                            -replace '(^)(\*[0-9,]{1,5}\) \!sKomm[ ]?)($)', '$1' <# Remove Excel Errors e.g. !sKomm #> `
                            -replace '([0-9])( [\-\+]{1,2})($)', '$1$3' <# Remove Plus or Minus #>
                        $count += 1
                    }
                    $wrline =  "$msmt_group - $m" `
                        -replace '([,"])(\*[0-9,]{1,5}\) )([0-9<>])', '$1$3' <# Remove Remarks e.g. *1) #> `
                        -replace '([,"])(\*[0-9,]{1,5}\) \!sKomm[ ]?)([,\n\r])', '$1' <# Remove Excel Errors e.g. !sKomm #> `
                        -replace '([0-9])( [\-\+]{1,2})([,\n\r])', '$1$3' <# Remove Plus or Minus #>
                    Add-Content -Path $OutPath -Value $wrline
                }
            }
        }
    }   
    #return $patient | ConvertTo-JSON
}


Function CleanCSV ($FileName, $indir = $fcsvout, $outdir = $ffiltout)
{
    $FilePath = "$indir\$FileName"
    $OutPath = "$outdir\" + (Get-Item $FilePath).BaseName + ".csv"

    $line = Get-Content $FilePath | Select-Object -First 1
    $delimiter = if($line.Split(",").Length -gt $line.Split(";").Length){","}else{";"};
    if ( $delimiter -eq ";" ) {
        Write-Host "Please note: Delimiter used is ; which will not work."
    }

    (Get-Content $FilePath -Raw) `
        -replace '([,"])(\*[0-9,]{1,5}\) )([0-9<>])', '$1$3' <# Remove Remarks e.g. *1) #> `
        -replace '([,"])(\*[0-9,]{1,5}\) \!sKomm[ ]?)([,\n\r])', '$1' <# Remove Excel Errors e.g. !sKomm #> `
        -replace '([0-9])( [\-\+]{1,2})([,\n\r])', '$1$3' <# Remove Plus or Minus #> | 
    Out-File $OutPath
}

Function ConvertCommaToSemikolon ($FileName, $indir = $ffiltout, $outdir = $fexcelcsvout) 
{
    $FilePath = "$indir\$FileName"
    $OutPath = "$outdir\" + (Get-Item $FilePath).BaseName + ".csv"
    
    # Convert CSV to ExcelUsable CSV
    #Import-Csv -Path $FilePath -Delimiter �,� | Export-Csv -Path $OutPath -Delimiter �;� -NoType # Does not work in this case as columns without title are not imported
    (Get-Content $FilePath -Raw -Encoding UTF8) `
        -replace ',', ';' <# Remove Remarks e.g. *1) #> |
    Out-File $OutPath -Encoding ascii

    #ToDo: Fix Umlaut issue
}

Function ExportFilteredJSON($patient, $FileName, $outdir = $fout){
    # ToDo: Convert every Patient's JSON to a Table


}


#endregion

#region init script
Write-Host "`n`nRunning MADatPars_Nadine, a utility written by Simon Monai...`n" -BackgroundColor DarkCyan
CheckDirs
#endregion

#region Convert XLSX to CSV
Write-Host "`n`nSTEP 1: Convert XLSX to CSV:`n" -ForegroundColor Yellow 
Write-Host "Searching for Files in $finput`n"
$excelFiles = Get-ChildItem -Path $finput -File -Include "*.xlsx" -Name
foreach ( $FileName in $excelFiles) {
    Write-Host "Converting $FileName.Name" -ForegroundColor Cyan
    ExcelToCsv -File "$FileName"
}
#endregion

#region Filter content
Write-Host "`n`nSTEP 2: Filter CSV:`n" -ForegroundColor Yellow
$csvFiles = Get-ChildItem -Path $fcsvout -File -Include "*.csv" -Name
foreach ( $FileName in $csvFiles) {
    Write-Host "Parsing $FileName" -ForegroundColor Cyan
    #ConvertCSV -File "$FileName" # Removes content unwillingly and has issues with umlauts
    FilterCSV -File "$FileName"
    #Write-Host "Filtering $FileName"
    #CleanCSV -File "$FileName"
    Write-Host "Converting $FileName to Excel CSV" -ForegroundColor Cyan
    ConvertCommaToSemikolon -File $FileName

}
#endregion

#region Combine Files
#endregion

#region 
Write-Host "`n`nCOMPLETED: Filtering Done Successfully :)`n" -ForegroundColor White -BackgroundColor DarkGreen
Write-Host "`n"
#endregion
