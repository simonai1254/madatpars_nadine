﻿$jsonobj = Get-Content -Raw -Path .\example.json | ConvertFrom-Json
$jsonobj



$dumb_array = @{ name = "Hans"; Geburtsdatum = "20211111" ; Measurements = @{Hämatogramm = 0}; "Differenzierung Maschinell" = @{Lymphozyten = 0; Neutrophyle = 1 } }



$objectdate = @{ 
    name = "Bärlauch"; 
    Geburtsdatum = "01011991"; 
    Fallnr = "12343";
    Measurements = @{
        "20211231" = @{
            "Hämatogramm - Thrombo" = 42
            "Diff Masch - Neutophy" = 22
        };
        "20220101" = @{
            "Hämatogramm - Thrombo" = 41
            "Serum Proteine - IGG" = 200
        }
    }
} 

$objectdate | ConvertTo-Json


$objectmsmt = @{ 
    name = "Bärlauch"; 
    Geburtsdatum = "01011991"; 
    Fallnr = "12343";
    Measurements = @{
        "Hämatogramm - Thrombo" = @{
            "20220101" = 42;
            "20211230" = 41;
        };
        "Diff Masch - Neutophy" = @{
            "20211231" = 22;
        }
    }
} 

$objectmsmt | ConvertTo-Json